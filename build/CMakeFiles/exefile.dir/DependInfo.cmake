# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/usama/Documents/git_practice/submodule_cmake/submodule_cmake/src/introd.cpp" "/home/usama/Documents/git_practice/submodule_cmake/submodule_cmake/build/CMakeFiles/exefile.dir/src/introd.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../ThirdParty/benchmark/include"
  "../ThirdParty/benchmark/src/../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/usama/Documents/git_practice/submodule_cmake/submodule_cmake/build/ThirdParty/benchmark/src/CMakeFiles/benchmark.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
