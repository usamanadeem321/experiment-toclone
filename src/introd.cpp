#include <benchmark/benchmark.h>
#include <iostream>

int add(int a, int b);
int mult(int a, int b);

int SomeFunction(int a, int b) { return a * b; }

static void BM_SomeFunction(benchmark::State &state) {
  // Perform setup here
  for (auto _ : state) {
    // This code gets timed
    std::cout << "SomeFunctions: " << SomeFunction(5, 20) << std::endl;
  }
}

int main() {
  std::cout << "In Introd.cpp new File" << std::endl;
  // BM_SomeFunction();

  return 0;
}